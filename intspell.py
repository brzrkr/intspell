from typing import Union


SUPPORTED_TYPES = (
    int,
    float,
)
FIGURE_NAMES = {
    0: "zero",
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five",
    6: "six",
    7: "seven",
    8: "eight",
    9: "nine",
    
    11: "eleven",
    12: "twelve",
    13: "thirteen",
    14: "fourteen",
    15: "fifteen",
    16: "sixteen",
    17: "seventeen",
    18: "eighteen",
    19: "nineteen",
    
    10: "ten",
    20: "twenty",
    30: "thirty",
    40: "forty",
    50: "fifty",
    60: "sixty",
    70: "seventy",
    80: "eighty",
    90: "ninety",
}


def spell_number(number: Union[int, float],
                 sep_name: str = "point",
                 emphasis: bool = False) -> str:
    """Spell out a number in letters.
    
    :param number: the number to spell out.
    :param sep_name: name of the decimal separator, "point" by default.
    :param emphasis: whether to emphasise words like "thousand" or "hundred"
                     by means of UPPERCASE.
    :return: the textual representation of `number` as a string.
    """
    
    if type(number) not in SUPPORTED_TYPES:
        raise NotImplementedError
    
    characteristic, dot, mantissa = str(number).partition(".")
    if mantissa and int(mantissa) == 0:
        mantissa = ""
    
    # Build characteristic.
    c_length = len(characteristic)
    if c_length == 0:
        c_text = FIGURE_NAMES[0]
    elif c_length == 1:
        c_text = FIGURE_NAMES[int(characteristic)]
    elif c_length == 2:
        if 9 < int(characteristic) < 20:
            c_text = FIGURE_NAMES[int(characteristic)]
        else:
            c_text = FIGURE_NAMES[int(characteristic[0]) * 10]
            unit = int(characteristic[1])
            if unit != "0":
                c_text += f"-{FIGURE_NAMES[unit]}"
    elif c_length == 3:
        hundreds = int(characteristic[0])
        
        c_text = f"{FIGURE_NAMES[hundreds]} " \
            f"{'HUNDRED' if emphasis else 'hundred'}"
        
        tens = int(characteristic[2])
        unit = int(characteristic[3])
        
        if 9 < int(characteristic) < 20:
            c_text = FIGURE_NAMES[int(characteristic)]
        else:
            c_text = FIGURE_NAMES[int(characteristic[0]) * 10]
            unit = int(characteristic[1])
            if unit != "0":
                c_text += f"-{FIGURE_NAMES[unit]}"
    else:
        raise NotImplementedError
    
    # Build mantissa
    if mantissa:
        m_text = ""
        for char in mantissa:
            m_text += f"{FIGURE_NAMES[int(char)]} "
    else:
        m_text = ""
    
    return (
        f"{c_text.rstrip()}"
        f"{f' {sep_name} ' if dot and m_text else ''}"
        f"{m_text.rstrip()}"
    )
