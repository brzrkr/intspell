from unittest import TestCase, main
from intspell import spell_number


class TestSimpleIntegers(TestCase):
    def test_units(self):
        self.assertEqual("one", spell_number(1))
        self.assertEqual("two", spell_number(2))
        self.assertEqual("three", spell_number(3))
        self.assertEqual("four", spell_number(4))
        self.assertEqual("five", spell_number(5))
        self.assertEqual("six", spell_number(6))
        self.assertEqual("seven", spell_number(7))
        self.assertEqual("eight", spell_number(8))
        self.assertEqual("nine", spell_number(9))
        self.assertEqual("zero", spell_number(0))
    
    def test_tens(self):
        self.assertEqual("ten", spell_number(10))
        self.assertEqual("twenty", spell_number(20))
        self.assertEqual("thirty", spell_number(30))
        self.assertEqual("forty", spell_number(40))
        self.assertEqual("fifty", spell_number(50))
        self.assertEqual("sixty", spell_number(60))
        self.assertEqual("seventy", spell_number(70))
        self.assertEqual("eighty", spell_number(80))
        self.assertEqual("ninety", spell_number(90))
    
    def test_hundreds(self):
        self.assertEqual("one hundred", spell_number(100))
        self.assertEqual("two hundred", spell_number(200))
        self.assertEqual("three hundred", spell_number(300))
        self.assertEqual("four hundred", spell_number(400))
        self.assertEqual("five hundred", spell_number(500))
        self.assertEqual("six hundred", spell_number(600))
        self.assertEqual("seven hundred", spell_number(700))
        self.assertEqual("eight hundred", spell_number(800))
        self.assertEqual("nine hundred", spell_number(900))
        
    def test_thousands(self):
        self.assertEqual("one thousand", spell_number(1_000))
        self.assertEqual("two thousand", spell_number(2_000))
        self.assertEqual("three thousand", spell_number(3_000))
        self.assertEqual("four thousand", spell_number(4_000))
        self.assertEqual("five thousand", spell_number(5_000))
        self.assertEqual("six thousand", spell_number(6_000))
        self.assertEqual("seven thousand", spell_number(7_000))
        self.assertEqual("eight thousand", spell_number(8_000))
        self.assertEqual("nine thousand", spell_number(9_000))

        self.assertEqual("ten thousand", spell_number(10_000))
        self.assertEqual("twenty thousand", spell_number(20_000))
        self.assertEqual("thirty thousand", spell_number(30_000))
        self.assertEqual("forty thousand", spell_number(40_000))
        self.assertEqual("fifty thousand", spell_number(50_000))
        self.assertEqual("sixty thousand", spell_number(60_000))
        self.assertEqual("seventy thousand", spell_number(70_000))
        self.assertEqual("eighty thousand", spell_number(80_000))
        self.assertEqual("ninety thousand", spell_number(90_000))

        self.assertEqual("one hundred thousand", spell_number(100_000))
        self.assertEqual("two hundred thousand", spell_number(200_000))
        self.assertEqual("three hundred thousand", spell_number(300_000))
        self.assertEqual("four hundred thousand", spell_number(400_000))
        self.assertEqual("five hundred thousand", spell_number(500_000))
        self.assertEqual("six hundred thousand", spell_number(600_000))
        self.assertEqual("seven hundred thousand", spell_number(700_000))
        self.assertEqual("eight hundred thousand", spell_number(800_000))
        self.assertEqual("nine hundred thousand", spell_number(900_000))
    
    def test_millions(self):
        self.assertEqual("one million", spell_number(1_000_000))
        self.assertEqual("two million", spell_number(2_000_000))
        self.assertEqual("three million", spell_number(3_000_000))
        self.assertEqual("four million", spell_number(4_000_000))
        self.assertEqual("five million", spell_number(5_000_000))
        self.assertEqual("six million", spell_number(6_000_000))
        self.assertEqual("seven million", spell_number(7_000_000))
        self.assertEqual("eight million", spell_number(8_000_000))
        self.assertEqual("nine million", spell_number(9_000_000))
    
        self.assertEqual("ten million", spell_number(10_000_000))
        self.assertEqual("twenty million", spell_number(20_000_000))
        self.assertEqual("thirty million", spell_number(30_000_000))
        self.assertEqual("forty million", spell_number(40_000_000))
        self.assertEqual("fifty million", spell_number(50_000_000))
        self.assertEqual("sixty million", spell_number(60_000_000))
        self.assertEqual("seventy million", spell_number(70_000_000))
        self.assertEqual("eighty million", spell_number(80_000_000))
        self.assertEqual("ninety million", spell_number(90_000_000))
    
        self.assertEqual("one hundred million", spell_number(100_000_000))
        self.assertEqual("two hundred million", spell_number(200_000_000))
        self.assertEqual("three hundred million", spell_number(300_000_000))
        self.assertEqual("four hundred million", spell_number(400_000_000))
        self.assertEqual("five hundred million", spell_number(500_000_000))
        self.assertEqual("six hundred million", spell_number(600_000_000))
        self.assertEqual("seven hundred million", spell_number(700_000_000))
        self.assertEqual("eight hundred million", spell_number(800_000_000))
        self.assertEqual("nine hundred million", spell_number(900_000_000))
    
    def test_billions(self):
        self.assertEqual("one billion", spell_number(1_000_000_000))
        self.assertEqual("two billion", spell_number(2_000_000_000))
        self.assertEqual("three billion", spell_number(3_000_000_000))
        self.assertEqual("four billion", spell_number(4_000_000_000))
        self.assertEqual("five billion", spell_number(5_000_000_000))
        self.assertEqual("six billion", spell_number(6_000_000_000))
        self.assertEqual("seven billion", spell_number(7_000_000_000))
        self.assertEqual("eight billion", spell_number(8_000_000_000))
        self.assertEqual("nine billion", spell_number(9_000_000_000))
    
        self.assertEqual("ten billion", spell_number(10_000_000_000))
        self.assertEqual("twenty billion", spell_number(20_000_000_000))
        self.assertEqual("thirty billion", spell_number(30_000_000_000))
        self.assertEqual("forty billion", spell_number(40_000_000_000))
        self.assertEqual("fifty billion", spell_number(50_000_000_000))
        self.assertEqual("sixty billion", spell_number(60_000_000_000))
        self.assertEqual("seventy billion", spell_number(70_000_000_000))
        self.assertEqual("eighty billion", spell_number(80_000_000_000))
        self.assertEqual("ninety billion", spell_number(90_000_000_000))
    
        self.assertEqual("one hundred billion", spell_number(100_000_000_000))
        self.assertEqual("two hundred billion", spell_number(200_000_000_000))
        self.assertEqual(spell_number(300_000_000_000), "three hundred "
                                                        "billion")
        self.assertEqual("four hundred billion", spell_number(400_000_000_000))
        self.assertEqual("five hundred billion", spell_number(500_000_000_000))
        self.assertEqual("six hundred billion", spell_number(600_000_000_000))
        self.assertEqual("seven hundred billion",
                         spell_number(700_000_000_000))
        self.assertEqual("eight hundred billion",
                         spell_number(800_000_000_000))
        self.assertEqual("nine hundred billion", spell_number(900_000_000_000))


class TestCompositeIntegers(TestCase):
    def test_teens(self):
        self.assertEqual("eleven", spell_number(11))
        self.assertEqual("twelve", spell_number(12))
        self.assertEqual("thirteen", spell_number(13))
        self.assertEqual("fourteen", spell_number(14))
        self.assertEqual("fifteen", spell_number(15))
        self.assertEqual("sixteen", spell_number(16))
        self.assertEqual("seventeen", spell_number(17))
        self.assertEqual("eighteen", spell_number(18))
        self.assertEqual("nineteen", spell_number(19))
    
    def test_tens(self):
        self.assertEqual("twenty-one", spell_number(21))
        self.assertEqual("thirty-one", spell_number(31))
        self.assertEqual("forty-one", spell_number(41))
        self.assertEqual("fifty-one", spell_number(51))
        self.assertEqual("sixty-one", spell_number(61))
        self.assertEqual("seventy-one", spell_number(71))
        self.assertEqual("eighty-one", spell_number(81))
        self.assertEqual("ninety-one", spell_number(91))
    
    def test_hundreds(self):
        self.assertEqual("one hundred one", spell_number(101))
        self.assertEqual("two hundred twenty", spell_number(220))
        self.assertEqual("three hundred thirty-three", spell_number(333))
    
    def test_thousands(self):
        self.assertEqual("two thousand one", spell_number(2_001))
        self.assertEqual("three thousand twenty", spell_number(3_020))
        self.assertEqual("four thousand three hundred", spell_number(4_300))
        self.assertEqual("five thousand four hundred five",
                         spell_number(5_405))
        self.assertEqual("six thousand five hundred sixty",
                         spell_number(6_560))
        self.assertEqual("seven thousand six hundred fifty-seven",
                         spell_number(7_657))
    
        self.assertEqual("thirty thousand two", spell_number(30_002))
        self.assertEqual("forty thousand thirty", spell_number(40_030))
        self.assertEqual("fifty thousand four hundred", spell_number(50_400))
        self.assertEqual("sixty-five thousand", spell_number(65_000))
        self.assertEqual("seventy-six thousand seven", spell_number(76_007))
        self.assertEqual("eighty-eight thousand ninety", spell_number(88_090))
        self.assertEqual("ninety thousand two hundred", spell_number(91_200))
        self.assertEqual("ninety-three thousand four hundred fifty",
                         spell_number(93_450))
        self.assertEqual("ninety-six thousand seven hundred eighty-nine",
                         spell_number(96_789))
    
        self.assertEqual("one hundred thousand nine", spell_number(100_009))
        self.assertEqual("two hundred thousand eighty", spell_number(200_080))
        self.assertEqual("three hundred thousand seven hundred",
                         spell_number(300_700))
        self.assertEqual("four hundred eight thousand", spell_number(408_000))
        self.assertEqual("five hundred ninety thousand", spell_number(590_000))
        self.assertEqual("six hundred ten thousand two", spell_number(610_002))
        self.assertEqual("seven hundred thirty thousand forty",
                         spell_number(730_040))
        self.assertEqual("eight hundred fifty thousand six hundred",
                         spell_number(850_600))
        self.assertEqual("nine hundred seventy-eight thousand",
                         spell_number(978_000))
        self.assertEqual("one hundred twenty-three thousand four",
                         spell_number(123_004))
        self.assertEqual("five hundred sixty-seven thousand eighty",
                         spell_number(567_080))
        self.assertEqual("nine hundred twelve thousand three hundred",
                         spell_number(912_300))
        self.assertEqual("four hundred fifty-six thousand "
                         "seven hundred eighty",
                         spell_number(456_780))
        self.assertEqual("nine hundred twelve thousand "
                         "three hundred forty-five",
                         spell_number(912_345))

    def test_millions(self):
        self.assertEqual("one million two hundred", spell_number(1_200_000))
        
        self.assertEqual("twenty-six million", spell_number(26_000_000))
        
        self.assertEqual("three hundred seventy-two million",
                         spell_number(372_000_000))
    
    def test_billions(self):
        self.assertEqual("seven billion thirty million",
                         spell_number(7_030_000_000))
        
        self.assertEqual("eighty-two billion four million",
                         spell_number(82_004_000_000))
        
        self.assertEqual("nine hundred fifteen billion three hundred million",
                         spell_number(915_300_000_000))


if __name__ == '__main__':
    main()
